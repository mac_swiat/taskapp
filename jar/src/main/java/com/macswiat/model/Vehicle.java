package com.macswiat.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "vehicles")
public class Vehicle extends TimeAudit{

    private Long id;
    private String vin;
    private String registrationNumber;
    private Owner owner;

    @Id
    @SequenceGenerator(name = "vehicle_ids", sequenceName = "vehicle_ids", allocationSize = 1)
    @GeneratedValue(generator = "vehicle_ids")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "vin", nullable = false, length = 17)
    @NotNull
    @Size(max = 17)
    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Column(name = "registration_number", nullable = false, length = 20)
    @NotNull
    @Size(max = 20)
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @ManyToOne
    @JoinColumn(name = "owner_id")
    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
