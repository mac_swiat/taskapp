package com.macswiat.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "owners")
public class Owner extends TimeAudit {

    private Long id;
    private String name;
    private Set<Vehicle> vehicles;


    @Id
    @SequenceGenerator(name = "owner_ids", sequenceName = "owner_ids", allocationSize = 1)
    @GeneratedValue(generator = "owner_ids")
    @NotNull
    @Column(name = "id", nullable = false, updatable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 100)
    @Size(max = 100)
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "owner")
    public Set<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
