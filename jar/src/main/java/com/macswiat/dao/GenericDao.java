package com.macswiat.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class GenericDao<T> {

    public static final Logger logger = LoggerFactory.getLogger(GenericDao.class);

    @PersistenceContext(name = "vehicle_ds")
    protected EntityManager entityManager;

    public void flush() { entityManager.flush();}

    public void save(T obj){
        entityManager.persist(obj);
    }

    public void update(T obj){
        entityManager.merge(obj);
    }

    public void remove(T obj){
        entityManager.remove(obj);
    }

}
