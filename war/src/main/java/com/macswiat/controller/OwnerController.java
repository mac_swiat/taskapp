package com.macswiat.controller;

import com.macswiat.dao.OwnersDao;
import com.macswiat.service.OwnersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ejb.ConcurrencyManagementType.BEAN;

@Singleton
@ConcurrencyManagement(BEAN)
@Path("/owner")
public class OwnerController {

    private static final Logger logger = LoggerFactory.getLogger(OwnerController.class);

    @EJB
    private OwnersService ownersService;

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOwners(){
        logger.info("OwnerController -> getAllOwners");
        return Response.ok(ownersService.getAllOwnersAsJsonArray()).build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createOwner(@FormParam("name") String name) {
        logger.info("OwnerController -> createOwner -> name : "+ name);
        if(ownersService.ownerExists(name)){
            return Response.status(Response.Status.BAD_REQUEST).entity("Owner with name " + name + " already exists").build();
        } else {
            ownersService.createOwner(name);
            return Response.status(Response.Status.CREATED).build();
        }
    }

    @GET
    @Path("/vehicles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOwnerVehicles(@QueryParam("owner") String owner){
        logger.info("OwnerController -> getOwnerVehicles");
        return Response.ok(ownersService.getOwnerVehiclesAsJsonArray(owner)).build();
    }



}
