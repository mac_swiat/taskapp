package com.macswiat.controller;

import com.macswiat.service.OwnersService;
import com.macswiat.service.VehiclesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ejb.ConcurrencyManagementType.BEAN;

@Singleton
@ConcurrencyManagement(BEAN)
@Path("/vehicle")
public class VehicleController {

    private static final Logger logger = LoggerFactory.getLogger(VehicleController.class);

    @EJB
    private VehiclesService vehiclesService;

    @EJB
    private OwnersService ownersService;

    @GET
    @Path("/all")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllVehicles(){
        logger.info("OwnerController -> getAllOwners");
        return Response.ok(vehiclesService.getAllVehiclesAsJsonArray(true)).build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createVehicle(@FormParam("vin") String vin, @FormParam("registrationNumber") String registrationNumber, @FormParam("owner") String owner) {
        logger.info("VehicleController -> createVehicle -> vin : "+ vin + "registrationNumber : "+registrationNumber + "owner : "+owner);
        if(ownersService.ownerExists(owner)){
            vehiclesService.createVehicle(vin, registrationNumber, owner);
            return Response.status(Response.Status.CREATED).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).entity("No owner with name "+ owner).build();
        }
    }

}
