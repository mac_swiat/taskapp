package com.macswiat;

import com.macswiat.controller.OwnerController;
import com.macswiat.controller.VehicleController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class App extends Application {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    @Override
    public Set<Class<?>> getClasses() {

        logger.info("IN APPLICATION");

        Set<Class<?>> classes = new HashSet<>();

        classes.add(VehicleController.class);
        classes.add(OwnerController.class);

        return classes;

    }
}
