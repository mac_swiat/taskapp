grant usage on schema public to app_user;

create table owners (
                      id int not null primary key,
                      name varchar(100) not null,
                      created timestamp not null,
                      updated timestamp
);

CREATE SEQUENCE owner_ids INCREMENT BY 1;

create table vehicles (
                        id int not null primary key,
                        vin varchar(17) not null,
                        registration_number varchar(20) not null,
                        owner_id int not null,
                        created timestamp not null,
                        updated timestamp,
                        foreign key (owner_id) references owners(id)
);

CREATE SEQUENCE vehicle_ids INCREMENT BY 1;

GRANT SELECT, INSERT, UPDATE ON TABLE owners TO app_user;
GRANT SELECT, INSERT, UPDATE ON TABLE vehicles TO app_user;
GRANT SELECT, INSERT, UPDATE ON TABLE vehicles TO app_user;

GRANT USAGE ON SEQUENCE owner_ids TO app_user;
GRANT USAGE ON SEQUENCE vehicle_ids TO app_user;